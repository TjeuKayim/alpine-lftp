Generate a docker image based on Alpine Linux with package openssh and lftp with **strict host key checking disabled**.
## StrictHostKeyChecking
For security reason, host key checking is not disabled. If you use this docker, you should add the host key in the `~/.ssh/known_hosts
` file.


The image is rebuilt each week -> See [project's pipelines](https://gitlab.com/capedev-labs/docker/alpine-lftp/pipelines)


## Get you  host key fingerprint

The command `ssh-keyscan -H  yourServer.domain.com`
Then you can copy  the result in a protected environment variable (`SERVER_FINGERPRINT`for instance)

## `.gitlab-ci.yml`

Example to copy a local folder `public` to distant server:
```yaml
deploy_master:
  image: registry.gitlab.com/capedev-labs/docker/alpine-lftp/master:latest
  stage: deploy
  script:
  - mkdir /root/.ssh
  # we suppose you use private/public ssh key ( stored in the variable ID_RSA. Add a blank line at the end of the variable "ID_RSA")
  - echo "$ID_RSA"  >  ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa
  - echo $SERVER_FINGERPRINT >  ~/.ssh/known_hosts
  # the , after FTP_USER is important to make lftp use your private key for authentication
  - lftp -e "mirror --delete -R  public/ $REMOTE_PROD_FOLDER;quit;" -u $FTP_USER, $REMOTE_FTP_URL
  only:
  - master
```